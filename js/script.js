$(function () {
  $("#revslider").revolution({
    delay: 9000,
    startwidth: 1140,
    startheight: 600,
    onHoverStop: "true",
    hideThumbs: 0,
    lazyLoad: "on",
    navigationType: "none",
    navigationHAlign: "center",
    navigationVAlign: "bottom",
    navigationHOffset: 0,
    navigationVOffset: 20,
    soloArrowLeftHalign: "left",
    soloArrowLeftValign: "center",
    soloArrowLeftHOffset: 0,
    soloArrowLeftVOffset: 0,
    soloArrowRightHalign: "right",
    soloArrowRightValign: "center",
    soloArrowRightHOffset: 0,
    soloArrowRightVOffset: 0,
    touchenabled: "on",
    stopAtSlide: -1,
    stopAfterLoops: -1,
    dottedOverlay: "twoxtwo",
    spinned: "spinner5",
    shadow: 0,
    hideTimerBar: "on",
    fullWidth: "off",
    fullScreen: "on",
    navigationStyle: "preview3",
  });

  $(document).on("click", ".js-linkBurger", function (e) {
    e.preventDefault();
    $(".js-linkBurger").toggleClass("open");
    $(".js-spMenu").toggleClass("open");
    if ($(this).hasClass("open")) {
      $("body").addClass("modal-open");
    } else {
      $("body").removeClass("modal-open");
    }
  });

  $(window).resize(function () {
    if ($(this).width() > 992) {
      if ($(".js-spMenu").hasClass("open")) {
        $("body").removeClass("modal-open");
        $(".js-spMenu").removeClass("open");
        $(".js-linkBurger").removeClass("open");
      }
    }
  });

  $(document).on("click", ".js-headerMenu", function (e) {
    let href = $(this).find("a").attr("href");
    $(this).siblings().removeClass("active");
    if (href.indexOf("#") !== -1) {
      e.preventDefault();
      $(this).toggleClass("active");
      let itemPosition = $(href).offset().top;
      $("html, body").animate({ scrollTop: itemPosition }, "slow");
    }
    if (href === "") {
      e.preventDefault();
      $("html, body").animate({ scrollTop: 0 }, "slow");
    }
    if ($(".js-spMenu").hasClass("open")) {
      $(".js-spMenu").removeClass("open");
      $(".js-linkBurger").removeClass("open");
      $("body").removeClass("modal-open");
    }
  });
});
